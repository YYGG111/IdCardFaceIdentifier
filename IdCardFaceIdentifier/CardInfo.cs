﻿using System;
using System.Text;

namespace IdCardFaceIdentifier
{
    public class CardInfo
    {
        public string PeopleName { get; set; }
        public string PeopleIDCode { get; set; }
        public string PeopleNation { get; set; }
        public string PeopleBirthday { get; set; }
        public string PeopleAddress { get; set; }
        public string ValidDate { get; set; }
        public string Department { get; set; }
        public string PeopleSex { get; set; }
        public byte[] PeopleImage { get; set; }

        override
        public string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendFormat("PeopleName:      {0}", PeopleName).AppendLine();
            sb.AppendFormat("PeopleIDCode:    {0}", PeopleIDCode).AppendLine();
            sb.AppendFormat("PeopleNation:    {0}", PeopleNation).AppendLine();
            sb.AppendFormat("ValidDate:       {0}", ValidDate).AppendLine();
            sb.AppendFormat("PeopleBirthday:  {0}", PeopleBirthday).AppendLine();
            sb.AppendFormat("PeopleAddress:   {0}", PeopleAddress).AppendLine();
            sb.AppendFormat("Department:      {0}", Department).AppendLine();
            sb.AppendFormat("PeopleSex:       {0}", PeopleSex).AppendLine();
            sb.AppendFormat("PeopleImage:     {0} byte", PeopleImage == null ? 0 : PeopleImage.Length).AppendLine();
            return sb.ToString();
        }

        internal void Clean()
        {
            PeopleName = null;
            PeopleIDCode = null;
            PeopleNation = null;
            PeopleBirthday = null;
            PeopleAddress = null;
            ValidDate = null;
            Department = null;
            PeopleSex = null;
            PeopleImage = null;
        }
    }
}
